export enum hours {
  MAX_HOURS = 96,
  PRE_MAX_HOURS = 72,
  MEDIUM_HOURS = 48,
  LOW_HOURS = 24,
}
