import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Course } from '../../entity/course/course.entity';

export interface CourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Course, 'id'>;
}
