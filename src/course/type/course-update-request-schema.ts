import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Course } from '../../entity/course/course.entity';

export interface CourseUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Course>;
}
