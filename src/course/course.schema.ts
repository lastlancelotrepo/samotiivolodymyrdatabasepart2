import Joi from 'joi';
import { Course } from '../entity/course/course.entity';

export const courseCreateSchema = Joi.object<Omit<Course, 'id'>>({
  name: Joi.string().required(),
  description: Joi.string().required(),
  hours: Joi.number().required(),
  lectors: Joi.number().optional(),
  students: Joi.number().optional(),
});

export const courseUpdateSchema = Joi.object<Partial<Course>>({
  name: Joi.string().optional(),
  description: Joi.string().optional(),
  hours: Joi.number().optional(),
  lectors: Joi.number().optional(),
  students: Joi.number().optional(),
});
