import { Router } from 'express';
import * as coursesController from './course.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { courseCreateSchema, courseUpdateSchema } from './course.schema';

const couresRouter = Router();

couresRouter.get('/', controllerWrapper(coursesController.getAllCourses));
couresRouter.get(
  '/:id',
  controllerWrapper(coursesController.getCourseById),
);

couresRouter.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);
couresRouter.patch(
  '/:id',
  validator.body(courseUpdateSchema),
  controllerWrapper(coursesController.updateCourseById),
);
couresRouter.delete(
  '/:id',
  controllerWrapper(coursesController.deleteCourseById),
);

export default couresRouter;
