import { NextFunction, Request, Response } from 'express';
import * as couresService from './course.service';
import { ValidatedRequest } from 'express-joi-validation';
import { Group } from '../entity/group/group.entity';
import { CourseCreateRequest } from './type/course-create-request-schema';
import { CourseUpdateRequest } from './type/course-update-request-schema';

export const getAllCourses = async (request: Request, response: Response) => {
  const groups = await couresService.getAllCourse();
  response.status(200).json(groups);
};

export const getCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const group = await couresService.getCourseById(id);
  response.status(200).json(group);
};

export const createCourse = async (
  request: ValidatedRequest<CourseCreateRequest>,
  response: Response,
) => {
  const groups = await couresService.createCourse(request.body);
  response.status(201).json(groups);
};

export const updateCourseById = async (
  request: ValidatedRequest<CourseUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await couresService.updateCourseById(id, request.body);
  response.status(200).json(group);
};

export const deleteCourseById = async (
  request: Request,
  response: Response,
) => {
  const { id } = request.params;
  const group = await couresService.deleteCourseById(id);
  response.status(200).json(group);
};
