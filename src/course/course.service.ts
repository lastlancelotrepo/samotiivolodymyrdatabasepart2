import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { Student } from '../entity/student/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Group } from '../entity/group/group.entity';
import { group } from 'console';
import { Course } from '../entity/course/course.entity';

const CourseRepository = AppDataSourse.getRepository(Course);

export const getAllCourse = async (): Promise<Course[]> => {
  const courses = CourseRepository.find({});

  return courses;
};

export const getCourseById = async (id: string) => {
  const course = await CourseRepository.createQueryBuilder('course')
    .leftJoinAndSelect('course.students', 'students')
    .where('course.id = :id', { id })
    .getOne();

  if (!course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this id not found',
    );
  }

  return await course;
};

export const createCourse = async (
  createCourseSchema: Omit<Course, 'id'>,
): Promise<Course> => {
  const course = await CourseRepository.findOne({
    where: {
      name: createCourseSchema.name,
    },
  });
  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exist',
    );
  }

  return await CourseRepository.save(createCourseSchema);
};

export const updateCourseById = async (
  id: string,
  courseUpdateSchema: Partial<Course>,
): Promise<UpdateResult> => {
  const result = await CourseRepository.update(id, courseUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this id not found',
    );
  }

  return result;
};

export const deleteCourseById = async (id: string): Promise<DeleteResult> => {
  const result = await CourseRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this id not found',
    );
  }
  return result;
};
