import { MigrationInterface, QueryRunner } from "typeorm";

export class AddNameIndexToStudentTable1688576027897 implements MigrationInterface {
    name = 'AddNameIndexToStudentTable1688576027897'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`CREATE INDEX "IDX_b5e856b621a7b64cdf48059067" ON "students" ("name") `);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b5e856b621a7b64cdf48059067"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

}
