import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeTypeOfRevilationInLectorAndCourse1688585659086 implements MigrationInterface {
    name = 'ChangeTypeOfRevilationInLectorAndCourse1688585659086'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0df9a2b79106263bbe926980ed"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fed6d4e40a7626912ab1c6e80c"`);
        await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "course_id"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_0c226038a877fcddda42045e6e6"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_fed6d4e40a7626912ab1c6e80cf" PRIMARY KEY ("coursesId")`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "lectorsId"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "coursesId"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "course_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "lector_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "lector_id"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "course_id"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "coursesId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_fed6d4e40a7626912ab1c6e80cf" PRIMARY KEY ("coursesId")`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD "lectorsId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_0c226038a877fcddda42045e6e6" PRIMARY KEY ("lectorsId", "coursesId")`);
        await queryRunner.query(`ALTER TABLE "lectors" ADD "course_id" integer`);
        await queryRunner.query(`CREATE INDEX "IDX_fed6d4e40a7626912ab1c6e80c" ON "lector_course" ("coursesId") `);
        await queryRunner.query(`CREATE INDEX "IDX_0df9a2b79106263bbe926980ed" ON "lector_course" ("lectorsId") `);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

}
