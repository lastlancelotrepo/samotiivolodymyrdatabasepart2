import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateCoursFieldHours1688581130298 implements MigrationInterface {
  name = 'UpdateCoursFieldHours1688581130298';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`,
    );
    await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "hours"`);
    await queryRunner.query(
      `CREATE TYPE "public"."courses_hours_enum" AS ENUM('96', '72', '48', '24')`,
    );
    await queryRunner.query(
      `ALTER TABLE "courses" ADD "hours" "public"."courses_hours_enum" NOT NULL DEFAULT '48'`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(`ALTER TABLE "lector_course" ADD PRIMARY KEY (id)`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`,
    );
    await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "hours"`);
    await queryRunner.query(`DROP TYPE "public"."courses_hours_enum"`);
    await queryRunner.query(
      `ALTER TABLE "courses" ADD "hours" integer NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP PRIMARY KEY (id)`,
    );
  }
}
