import { MigrationInterface, QueryRunner } from "typeorm";

export class CreatedAllTableForHomeworkAndRevilations1688564805323 implements MigrationInterface {
    name = 'CreatedAllTableForHomeworkAndRevilations1688564805323'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "lectors" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "course_id" integer, CONSTRAINT "PK_87eda9bf8c85d84a6b18dfc4991" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "courses" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "description" character varying NOT NULL, "hours" integer NOT NULL, CONSTRAINT "PK_3f70a487cc718ad8eda4e6d58c9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "mark" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "mark" integer NOT NULL, "student_id" integer NOT NULL, "course_id" integer NOT NULL, "lector_id" integer NOT NULL, CONSTRAINT "PK_0c6d4afd73cc2b4eee5a926aafc" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "students" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying, "surname" character varying, "email" character varying, "age" numeric, "image_path" character varying, "group_id" integer NOT NULL, "course_id" integer, CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "groups" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, CONSTRAINT "PK_659d1483316afb28afd3a90646e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "lector_course" ("lectorsId" integer NOT NULL, "coursesId" integer NOT NULL, CONSTRAINT "PK_0c226038a877fcddda42045e6e6" PRIMARY KEY ("lectorsId", "coursesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_0df9a2b79106263bbe926980ed" ON "lector_course" ("lectorsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_fed6d4e40a7626912ab1c6e80c" ON "lector_course" ("coursesId") `);
        await queryRunner.query(`ALTER TABLE "mark" ADD CONSTRAINT "FK_b2cba0d2e6636758313f34ede91" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mark" ADD CONSTRAINT "FK_5bb91feed8fe35d34fed039a301" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mark" ADD CONSTRAINT "FK_6c3fea7f6ffb93af51d74830c28" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "students" ADD CONSTRAINT "FK_2a3ddf4de242a7cba3309a9acd6" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`);
        await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_2a3ddf4de242a7cba3309a9acd6"`);
        await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`);
        await queryRunner.query(`ALTER TABLE "mark" DROP CONSTRAINT "FK_6c3fea7f6ffb93af51d74830c28"`);
        await queryRunner.query(`ALTER TABLE "mark" DROP CONSTRAINT "FK_5bb91feed8fe35d34fed039a301"`);
        await queryRunner.query(`ALTER TABLE "mark" DROP CONSTRAINT "FK_b2cba0d2e6636758313f34ede91"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fed6d4e40a7626912ab1c6e80c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0df9a2b79106263bbe926980ed"`);
        await queryRunner.query(`DROP TABLE "lector_course"`);
        await queryRunner.query(`DROP TABLE "groups"`);
        await queryRunner.query(`DROP TABLE "students"`);
        await queryRunner.query(`DROP TABLE "mark"`);
        await queryRunner.query(`DROP TABLE "courses"`);
        await queryRunner.query(`DROP TABLE "lectors"`);
    }

}
