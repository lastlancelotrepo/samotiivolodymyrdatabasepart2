import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../student/student.router';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.fiter';
import path from 'path';
import auth from './middlewares/auth.middleware';
import { AppDataSourse } from '../configs/database/data-source';
import groupsRouter from '../group/group.router';
import lectorRouter from '../lector/lector.router';
import couresRouter from '../course/course.router';
import markRouter from '../mark/mark.router';
import lectorCourseRouter from '../lector_course/lector-course.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);
//app.use(auth);
AppDataSourse.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((e) => {
    console.log('Typeorm failed to connect to database', e);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/lectors', lectorRouter);
app.use('/api/v1/courses', couresRouter);
app.use('/api/v1/mark', markRouter);
app.use('/api/v1/lector_course', lectorCourseRouter);

app.use(exceptionFilter);

export default app;
