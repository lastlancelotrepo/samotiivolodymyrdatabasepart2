import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Lector } from '../entity/lector/lector.entity';

const lectorsRepository = AppDataSourse.getRepository(Lector);

export const getAllLector = async (): Promise<Lector[]> => {
  const students = lectorsRepository.find({});

  return await lectorsRepository.find({});
};

export const getLectorById = async (id: string) => {
  const student = await lectorsRepository
    .createQueryBuilder('lector')
    .select([
      'lector.id as "id"',
      'lector.name as "name"',
      'lector.email as "email"',
      'lector.password as "password"',
    ])
    .leftJoinAndSelect('lector.courses', 'courses')
    .leftJoinAndSelect('course.students', 'students')
    .where('lector.id = :id', { id })
    .getOne();

  if (!student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Stundent with this id not found',
    );
  }

  return await student;
};

export const createLector = async (
  createLectorSchema: Omit<Lector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      email: createLectorSchema.email,
    },
  });
  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this email already exist',
    );
  }

  return await lectorsRepository.save(createLectorSchema);
};

export const updateLectorById = async (
  id: string,
  lectorUpdateSchema: Partial<Lector>,
): Promise<UpdateResult> => {
  const result = await lectorsRepository.update(id, lectorUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this id not found',
    );
  }

  return result;
};

export const deleteLectorById = async (id: string): Promise<DeleteResult> => {
  const result = await lectorsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this id is not found',
    );
  }
  return result;
};
