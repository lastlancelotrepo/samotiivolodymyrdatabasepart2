import { Router } from 'express';
import * as lectorsController from './lector.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { lectorCreateSchema, lectorUpdateSchema } from './lector.schema';

const lectorRouter = Router();

lectorRouter.get('/', controllerWrapper(lectorsController.getAllLectors));
lectorRouter.get('/:id', controllerWrapper(lectorsController.getLectorById));

lectorRouter.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);
lectorRouter.patch(
  '/:id',
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorsController.updateLectorById),
);
lectorRouter.delete(
  '/:id',
  controllerWrapper(lectorsController.deleteLectorById),
);

export default lectorRouter;
