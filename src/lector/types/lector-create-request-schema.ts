import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Lector } from '../../entity/lector/lector.entity';

export interface LectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Lector>;
}
