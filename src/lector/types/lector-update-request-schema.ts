import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Lector } from '../../entity/lector/lector.entity';

export interface LectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Lector, 'id'>;
}
