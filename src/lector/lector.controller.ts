import { NextFunction, Request, Response, response } from 'express';
import * as lectorsService from './lector.servise';
import { ValidatedRequest } from 'express-joi-validation';
import { LectorCreateRequest } from './types/lector-update-request-schema';
import { LectorUpdateRequest } from './types/lector-create-request-schema';

export const getAllLectors = async (request: Request, response: Response) => {
  const students = await lectorsService.getAllLector();
  response.status(200).json(students);
};

export const getLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const student = await lectorsService.getLectorById(id);
  response.status(200).json(student);
};

export const createLector = async (
  request: ValidatedRequest<LectorCreateRequest>,
  response: Response,
) => {
  const students = await lectorsService.createLector(request.body);
  response.status(201).json(students);
};

export const updateLectorById = async (
  request: ValidatedRequest<LectorUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await lectorsService.updateLectorById(
    request.params.id,
    request.body,
  );
  response.status(200).json(student);
};

export const deleteLectorById = async (
  request: Request,
  response: Response,
) => {
  const { id } = request.params;
  const student = await lectorsService.deleteLectorById(id);
  response.status(200).json(student);
};
