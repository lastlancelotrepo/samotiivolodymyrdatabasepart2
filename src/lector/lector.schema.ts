import Joi from 'joi';
import { Lector } from '../entity/lector/lector.entity';

export const lectorCreateSchema = Joi.object<Omit<Lector, 'id'>>({
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
  courses: Joi.number().optional(),
});

export const lectorUpdateSchema = Joi.object<Partial<Lector>>({
  name: Joi.string().optional(),
  email: Joi.string().optional(),
  password: Joi.string().optional(),
  courses: Joi.number().optional(),
});
