import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { Student } from '../entity/student/student.entity';
import { Mark } from '../entity/mark/mark.entity';

const marksRepository = AppDataSourse.getRepository(Mark);
const studentsRepository = AppDataSourse.getRepository(Student);

export const getAllMarkForStudent = async (id: number) => {
  const student = await marksRepository
    .createQueryBuilder('mark')
    .select('mark.mark as "mark"')
    .leftJoin('mark.course', 'course')
    .addSelect('course.name as "course"')
    .leftJoin('mark.student', 'student')
    .addSelect('student.name as "student"')
    .where('student.id = :id', { id })
    .getRawMany();

  if (!student || student.length === 0) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this id doesn`t exist or doesn`t have any mark',
    );
  }
  return await student;
};

export const getAllMarkForCourse = async (id: number) => {
  const student = await marksRepository
    .createQueryBuilder('mark')
    .select('mark.mark as "mark"')
    .leftJoin('mark.student', 'student')
    .addSelect('student.name as "student"')
    .leftJoin('mark.lector', 'lector')
    .addSelect('lector.name as "lector"')
    .leftJoin('mark.course', 'course')
    .addSelect('course.name as "course"')
    .where('course.id = :id', { id })
    .getRawMany();

  if (!student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this id doesn`t exist or doesn`t have any mark',
    );
  }
  return await student;
};

export const AddMarkForStudent = async (
  createMarkSchema: Omit<Mark, 'id'>,
): Promise<Mark> => {
  return await marksRepository.save(createMarkSchema);
};
