import { Router } from 'express';
import * as marksController from './mark.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { studentAddMarkSchema } from './mark.schema';

const markRouter = Router();

markRouter.get(
  '/student/:id',
  controllerWrapper(marksController.getAllMarkForStudent),
);

markRouter.get(
  '/course/:id',
  controllerWrapper(marksController.getAllMarkForCourse),
);

markRouter.post(
  '/',
  validator.body(studentAddMarkSchema),
  controllerWrapper(marksController.createMarkForStudent),
);

export default markRouter;
