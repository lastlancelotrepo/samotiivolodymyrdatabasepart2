import { Request, Response } from 'express';
import * as marksService from './mark.service';
import { ValidatedRequest } from 'express-joi-validation';
import { markCreateRequest } from './type/mark.create-request-schema';

export const getAllMarkForStudent = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const students = await marksService.getAllMarkForStudent(request.params.id);
  response.status(200).json(students);
};

export const getAllMarkForCourse = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const students = await marksService.getAllMarkForCourse(request.params.id);
  response.status(200).json(students);
};

export const createMarkForStudent = async (
  request: ValidatedRequest<markCreateRequest>,
  response: Response,
) => {
  const students = await marksService.AddMarkForStudent(request.body);
  response.status(201).json(students);
};
