import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Mark } from '../../entity/mark/mark.entity';

export interface markCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Mark, 'id'>;
}
