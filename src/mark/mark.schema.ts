import Joi from 'joi';
import { Mark } from '../entity/mark/mark.entity';

export const studentAddMarkSchema = Joi.object<Partial<Mark>>({
  mark: Joi.number().required(),
  studentId: Joi.number().required(),
  courseId: Joi.number().required(),
  lectorId: Joi.number().required(),
});
