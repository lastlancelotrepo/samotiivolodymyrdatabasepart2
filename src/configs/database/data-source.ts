import { DataSource } from "typeorm";
import { databaseconfiguration } from "./database-config";

export const AppDataSourse = new DataSource(databaseconfiguration());