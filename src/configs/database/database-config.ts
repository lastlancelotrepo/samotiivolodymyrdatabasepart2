import { DataSource, DataSourceOptions, Entity } from 'typeorm';
import { env } from 'process';

export const databaseconfiguration = (isMigrationRun = true): DataSourceOptions => {
    const ROOT_PATH: string = process.cwd();
    const migrationPath = `${ROOT_PATH}/**/migrations/*{.ts,.js}`;
    const entitiesPath = `${ROOT_PATH}/**/*.entity{.ts,.js}`;

    return {
        type: "postgres",
        host: process.env.DATABASE_HOST,
        port: Number(process.env.DATABASE_PORT),
        username: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_DATABASE,
        entities: [entitiesPath],
        migrations: [migrationPath],
        migrationsTableName: 'migrations',
        migrationsRun: isMigrationRun,
        logging: true,
        synchronize: true,
    }
}