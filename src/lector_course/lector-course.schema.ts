import Joi from 'joi';
import { Lector_Course } from '../entity/lector_course/lector_course.entity';

export const lectorUpdateCourseSchema = Joi.object<Partial<Lector_Course>>({
  courseId: Joi.number().required(),
  lectorId: Joi.number().required(),
});
