import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { Lector_Course } from '../entity/lector_course/lector_course.entity';

const lector_coursesRepository = AppDataSourse.getRepository(Lector_Course);

export const getAllCourseForLector = async (id: number) => {
  const courses = await lector_coursesRepository
    .createQueryBuilder('lector_course')
    .select('lector_course.id as "id"')
    .leftJoinAndSelect('lector_course.lector', 'lector')
    .leftJoin('lector_course.course', 'course')
    .leftJoin('course.students', 'student')
    .addSelect([
      'student.name as "student_name"',
      'student.surname as "student_surname"',
    ])
    .where('lector.id = :id', { id })
    .getRawMany();

  if (!courses || courses.length === 0) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this id doesn`t exist or doesn`t have any course',
    );
  }
  return await courses;
};

export const getAllMarkForCourse = async (id: number) => {
  const student = await lector_coursesRepository
    .createQueryBuilder('mark')
    .select('mark.mark as "mark"')
    .leftJoin('mark.student', 'student')
    .addSelect('student.name as "student"')
    .leftJoin('mark.lector', 'lector')
    .addSelect('lector.name as "lector"')
    .leftJoin('mark.course', 'course')
    .addSelect('course.name as "course"')
    .where('course.id = :id', { id })
    .getRawMany();

  if (!student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this id doesn`t exist or doesn`t have any mark',
    );
  }
  return await student;
};

export const AddLectorToCourse = async (
  createLector_CourseSchema: Omit<Lector_Course, 'id'>,
): Promise<Lector_Course> => {
  return await lector_coursesRepository.save(createLector_CourseSchema);
};
