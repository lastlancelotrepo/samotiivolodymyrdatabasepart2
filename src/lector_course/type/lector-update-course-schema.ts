import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Lector_Course } from '../../entity/lector_course/lector_course.entity';

export interface lectorCourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Lector_Course, 'id'>;
}
