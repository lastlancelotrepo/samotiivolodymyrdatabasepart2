import { Request, Response } from 'express';
import * as lector_coursesService from './lector-course.service';
import { ValidatedRequest } from 'express-joi-validation';
import { lectorCourseCreateRequest } from './type/lector-update-course-schema';

export const getAllCourseForLector = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const students = await lector_coursesService.getAllCourseForLector(
    request.params.id,
  );
  response.status(200).json(students);
};

export const AddLectorToCourse = async (
  request: ValidatedRequest<lectorCourseCreateRequest>,
  response: Response,
) => {
  const students = await lector_coursesService.AddLectorToCourse(request.body);
  response.status(201).json(students);
};
