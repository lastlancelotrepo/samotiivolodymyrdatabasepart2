import { Router } from 'express';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { lectorUpdateCourseSchema } from './lector-course.schema';
import * as lectorCourseController from './lector-course.controller';

const lectorCourseRouter = Router();

lectorCourseRouter.get(
  '/lector/:id',
  controllerWrapper(lectorCourseController.getAllCourseForLector),
);

lectorCourseRouter.post(
  '/lector_course/',
  validator.body(lectorUpdateCourseSchema),
  controllerWrapper(lectorCourseController.AddLectorToCourse),
);

export default lectorCourseRouter;
