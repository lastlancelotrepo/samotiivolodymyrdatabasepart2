import { Router } from 'express';
import * as studentsController from './student.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateCourseSchema,
  studentUpdateSchema,
} from './student.schema';

const studentRouter = Router();

studentRouter.get('/:id', controllerWrapper(studentsController.getStudentById));

studentRouter.get('/', controllerWrapper(studentsController.getStudentByName));

studentRouter.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);
studentRouter.patch(
  '/:id',
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

studentRouter.patch(
  '/course/:id',
  validator.body(studentUpdateCourseSchema),
  controllerWrapper(studentsController.updateCourseIdForStudentById),
);

/*
studentRouter.patch(
  '/:id/image',
  uploadMiddleware.single('file'),
  validator.params(idParamSchema),
  controllerWrapper(studentsController.addImage),
);
studentRouter.patch(
  '/:id/group',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.addGroupToStudent),
);
*/
studentRouter.delete(
  '/:id',
  controllerWrapper(studentsController.deleteStudentById),
);

export default studentRouter;
