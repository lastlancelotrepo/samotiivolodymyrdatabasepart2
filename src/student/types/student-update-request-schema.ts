import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Student } from '../../entity/student/student.entity';

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Student>;
}
