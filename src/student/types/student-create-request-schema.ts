import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Student } from '../../entity/student/student.entity';

export interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Student, 'id'>;
}
