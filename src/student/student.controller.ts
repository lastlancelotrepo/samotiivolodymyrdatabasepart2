import { NextFunction, Request, Response, response } from 'express';
import * as studentsService from './student.servise';
import { IStudentUpdateRequest } from './types/student-update-request-schema';
import { IStudentCreateRequest } from './types/student-create-request-schema';
import { ValidatedRequest } from 'express-joi-validation';

export const getAllStudent = async (request: Request, response: Response) => {
  const students = await studentsService.getAllStudent();
  response.status(200).json(students);
};

export const getStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.status(200).json(student);
};

export const getStudentByName = async (
  request: Request<{ name: string }>,
  response: Response,
  next: NextFunction,
) => {
  const name = request.query.name as string;
  const student = await studentsService.getStudentByName(name);
  response.status(200).json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const students = await studentsService.createStudent(request.body);
  response.status(201).json(students);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(
    request.params.id,
    request.body,
  );
  response.status(200).json(student);
};

export const updateCourseIdForStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateCourseForStudentById(
    request.params.id,
    request.body,
  );
  response.status(200).json(student);
};

export const deleteStudentById = async (
  request: Request,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.status(200).json(student);
};
