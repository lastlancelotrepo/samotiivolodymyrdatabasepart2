import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { Student } from '../entity/student/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

const studentsRepository = AppDataSourse.getRepository(Student);

export const getAllStudent = async (): Promise<Student[]> => {
  const students = studentsRepository.find({});

  return await studentsRepository.find({});
};

export const getStudentById = async (id: number) => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as "id"',
      'student.name as "name"',
      'student.surname as "surname"',
      'student.email as "email"',
      'student.age as "age"',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Stundent with this id not found',
    );
  }

  return await student;
};

export const getStudentByName = async (name: string) => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as "id"',
      'student.name as "name"',
      'student.surname as "surname"',
      'student.email as "email"',
      'student.age as "age"',
    ])
    .where('student.name = :name', { name })
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .getRawMany();

  if (!student || student.length === 0) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Stundent with this name not found',
    );
  }

  return await student;
};

export const createStudent = async (
  createStudentSchema: Omit<Student, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student woth this email already exist',
    );
  }

  return await studentsRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  id: string,
  studentUpdateSchema: Partial<Student>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, studentUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this id not found',
    );
  }

  return result;
};

export const updateCourseForStudentById = async (
  id: string,
  studentUpdateSchema: Partial<Student>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, studentUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this id not found',
    );
  }

  return result;
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'Student not found');
  }
  return result;
};
