import Joi from 'joi';
import { Student } from '../entity/student/student.entity';

export const studentCreateSchema = Joi.object<Omit<Student, 'id'>>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  email: Joi.string().required(),
  age: Joi.number().required(),
  groupId: Joi.number().required(),
  courseId: Joi.number().optional(),
});

export const studentUpdateSchema = Joi.object<Partial<Student>>({
  name: Joi.string().optional(),
  surname: Joi.string().optional(),
  email: Joi.string().optional(),
  age: Joi.number().optional(),
  groupId: Joi.number().optional(),
  courseId: Joi.number().optional(),
});

export const studentUpdateCourseSchema = Joi.object<Partial<Student>>({
  courseId: Joi.number().required(),
});
