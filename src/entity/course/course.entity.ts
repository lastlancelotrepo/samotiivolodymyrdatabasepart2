import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Lector } from '../lector/lector.entity';
import { CoreEntity } from '../../application/entities/core.entity';
import { Mark } from '../mark/mark.entity';
import { Student } from '../student/student.entity';
import { hours } from '../../course/enums/hours.enum';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column()
  name: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: hours,
    name: 'hours',
    default: hours.MEDIUM_HOURS,
  })
  hours: number;

  @OneToMany(() => Lector, (lector) => lector.courses, {
    nullable: true,
    eager: false,
  })
  lectors: Lector[];

  @OneToMany(() => Student, (student) => student.course)
  students: Student[];

  @OneToMany(() => Mark, (mark) => mark.course, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'mark_id' })
  marks: Mark[];
}
