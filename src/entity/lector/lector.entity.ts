import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../course/course.entity';
import { Mark } from '../mark/mark.entity';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Course, (courses) => courses.lectors, {
    nullable: true,
    eager: false,
  })
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'mark_id' })
  marks: Mark[];
}
