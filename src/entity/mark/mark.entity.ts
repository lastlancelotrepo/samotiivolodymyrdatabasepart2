import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../group/group.entity';
import { Course } from '../course/course.entity';
import { Lector } from '../lector/lector.entity';
import { Student } from '../student/student.entity';

@Entity({ name: 'mark' })
export class Mark extends CoreEntity {
  @Column({
    name: 'mark',
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'student_id',
  })
  studentId: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;
  @Column({
    type: 'integer',
    nullable: false,
    name: 'course_id',
  })
  courseId: number;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: number;
}
