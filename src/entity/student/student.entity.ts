import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../group/group.entity';
import { Mark } from '../mark/mark.entity';
import { Course } from '../course/course.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  @Index()
  name: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: true,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'image_path',
  })
  image_path: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'group_id',
  })
  groupId: number;

  @ManyToOne(() => Course, (course) => course.students)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'course_id',
  })
  courseId: number;

  @OneToMany(() => Mark, (mark) => mark.student, {
    nullable: true,
    eager: false,
  })
  marks: Mark[];
}
