import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  TableForeignKey,
  UpdateDateColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../lector/lector.entity';
import { Course } from '../course/course.entity';

@Entity({ name: 'lector_course' })
export class Lector_Course extends CoreEntity {
  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;
  @Column({
    type: 'integer',
    nullable: false,
    name: 'course_id',
  })
  courseId: number;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: number;
}
