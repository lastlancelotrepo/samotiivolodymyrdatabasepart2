import { NextFunction, Request, Response } from 'express';
import * as groupsService from './group.service';
import { IGroupCreateRequest } from './types/group-create-request-schema';
import { IGroupUpdateRequest } from './types/group-update-request-schema';
import { ValidatedRequest } from 'express-joi-validation';

export const getAllGroup = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroup();
  response.status(200).json(groups);
};

export const getGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const group = await groupsService.getGroupById(id);
  response.status(200).json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const groups = await groupsService.createGroup(request.body);
  response.status(201).json(groups);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.status(200).json(group);
};

export const deleteGroupById = async (request: Request, response: Response) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.status(200).json(group);
};
