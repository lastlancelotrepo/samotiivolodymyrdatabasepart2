import { Router } from 'express';
import * as groupsController from './group.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { groupCreateSchema, groupUpdateSchema } from './group.schema';

const groupsRouter = Router();

groupsRouter.get('/', controllerWrapper(groupsController.getAllGroup));
groupsRouter.get('/:id', controllerWrapper(groupsController.getGroupById));

groupsRouter.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);
groupsRouter.patch(
  '/:id',
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);
groupsRouter.delete(
  '/:id',
  controllerWrapper(groupsController.deleteGroupById),
);

export default groupsRouter;
