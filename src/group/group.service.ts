import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enums';
import { AppDataSourse } from '../configs/database/data-source';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Group } from '../entity/group/group.entity';
const groupRepository = AppDataSourse.getRepository(Group);

export const getAllGroup = async (): Promise<Group[]> => {
  const groups = groupRepository.find({});

  return groups;
};

export const getGroupById = async (id: string) => {
  const group = await groupRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'students')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this id not found',
    );
  }

  return await group;
};

export const createGroup = async (
  createGroupSchema: Omit<Group, 'id'>,
): Promise<Group> => {
  const group = await groupRepository.findOne({
    where: {
      name: createGroupSchema.name,
    },
  });
  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exist',
    );
  }

  return await groupRepository.save(createGroupSchema);
};

export const updateGroupById = async (
  id: string,
  groupUpdateSchema: Partial<Group>,
): Promise<UpdateResult> => {
  const result = await groupRepository.update(id, groupUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this id not found',
    );
  }

  return result;
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const result = await groupRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this id not found',
    );
  }
  return result;
};
