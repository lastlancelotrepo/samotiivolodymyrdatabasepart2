import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Group } from '../../entity/group/group.entity';

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Group>;
}
