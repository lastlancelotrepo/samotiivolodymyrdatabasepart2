import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Group } from '../../entity/group/group.entity';

export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Group, 'id'>;
}
